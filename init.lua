--------------------------------------------------------
-- Minetest :: Digital LED Mod v1.2 (digital)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2016-2018, Leslie Ellen Krause
--
-- ./games/just_test_tribute/mods/digital/init.lua
--------------------------------------------------------

local end_time = 1514764800
local flasher_timeout = 30
local rainbow_timeout = 21600

-- partyorn https://freesound.org/people/soundstack/sounds/203246/
-- balloons https://freesound.org/people/mrmayo/sounds/77039/

----------------------------------------------------------------------------

function on_counter_timer( pos, elapsed )
	local channel = minetest.get_meta( pos ):get_int( "channel" )
	local param2 = minetest.get_node( pos ).param2
	local digit

	if channel > 4 then
		local timer = math.floor( core.get_timeofday( ) * 1440 )

		if channel == 5 then
			digit = math.floor( timer / 60 / 10 )
		elseif channel == 6 then
			digit = math.floor( timer / 60 % 10 ) 
		elseif channel == 7 then
			digit = math.floor( timer % 60 / 10 )
		elseif channel == 8 then
			digit = timer % 60 % 10
		end
	else
		local timer = end_time - os.time( )

		if timer >= 3600 then
			digit = ( { 6, 0, 0, 0 } )[ channel ]
		elseif timer >= 0 then
			if channel == 1 then
				digit = math.floor( timer / 60 / 10 )
			elseif channel == 2 then
				digit = math.floor( timer / 60 % 10 ) 
			elseif channel == 3 then
				digit = math.floor( timer % 60 / 10 )
			elseif channel == 4 then
				digit = timer % 60 % 10
			end
		elseif timer >= -10 then
			digit = timer % 2 == 0 and 0 or "nil"
		else
			digit = ( { 2, 0, 1, 9 } )[ channel ]
		end
	end
	minetest.swap_node( pos, { name = "digital:counter_" .. digit, param2 = param2 } )

	return true
end

function on_counter_reset( pos, node, player )
	if minetest.check_player_privs( player:get_player_name( ), { superuser = true } ) then
		local meta = minetest.get_meta( pos )
		local channel = meta:get_int( "channel" )

		channel = channel == 8 and 0 or channel + 1

		if channel == 0 then
			minetest.get_node_timer( pos ):stop( )
			minetest.swap_node( pos, { name = "digital:counter_off", param2 = node.param2 } )
		else
			minetest.get_node_timer( pos ):start( 1 )
			minetest.swap_node( pos, { name = "digital:counter_8", param2 = node.param2 } )
		end

		meta:set_int( "channel", channel )
		minetest.chat_send_player( player:get_player_name( ), string.format( "Display set to channel #%d.", channel ) )
	end
end

----------------------------------------------------------------------------

function register_counter( name, tile, def )
	def.description = "LED Counter"
	def.drawtype = "nodebox"
	def.tiles = {
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
       	        tile
        }
	def.paramtype2 = "facedir"
	def.node_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.5, 0.25, 0.5, 0.5, 0.5 },
		},
	}
	def.groups = { cracky = 1 }
	def.sounds = default.node_sound_metal_defaults( )

	minetest.register_node( "digital:counter_" .. name, def	)
end

function register_rainbow( name, tile, anim, def )
	def.description = "LED Rainbow"
	def.inventory_image = "rainbow_inv.png"
	def.drawtype = "nodebox"
	def.tiles = {
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		anim == 0 and tile or { name = tile, animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = anim } }
	}
       	def.paramtype2 = "facedir"
	def.node_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.12, 0.3, 0.5, 0.18, 0.5 },
		--	{ -0.5, -0.15, 0.3, 0.5, 0.15, 0.5 },
		},
	}
	def.groups = { cracky = 1 }
	def.sounds = default.node_sound_metal_defaults( )

	minetest.register_node( "digital:rainbow_" .. name, def )
end

function register_flasher( name, tile, anim, def )
	def.description = "LED Flasher"
	def.inventory_image = "flasher_inv.png"
	def.drawtype = "nodebox"
	def.tiles = {
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		"digital_box.png",
		anim == 0 and tile or { name = tile, animation = { type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = anim } }
	}
       	def.paramtype2 = "facedir"
	def.node_box = {
		type = "fixed",
		fixed = {
			{ -0.5, -0.12, 0.3, 0.5, 0.18, 0.5 },
	--		{ -0.5, -0.15, 0.3, 0.5, 0.15, 0.5 },
		},
	}
	def.groups = { cracky = 1 }
	def.sounds = default.node_sound_metal_defaults( )

	minetest.register_node( "digital:flasher_" .. name, def )
end

----------------------------------------------------------------------------

register_counter( "sep", "counter_sep.png", { light_source = 10 } )

register_counter( "off", "counter_off.png", {
	on_rightclick = on_counter_reset,
	after_place_node = function( pos, player, itemstack )
		minetest.get_meta( pos ):set_int( "channel", 0 )
        end,
} )

register_counter( "nil", "counter_nil.png", {
	light_source = 10,
	drop = "digital:counter_off",
	on_timer = on_counter_timer,
	on_rightclick = on_counter_reset,
} )

for digit = 0, 9 do
	register_counter( digit, "counter_" .. digit .. ".png", {
		light_source = 10,
		drop = "digital:counter_off",
		on_timer = on_counter_timer,
		on_rightclick = on_counter_reset,
	} )
end

register_rainbow( "on", "rainbow_on.png", 10.0, {
	light_source = 10,
	drop = "digital:rainbow_off",
	on_timer = function ( pos, elapsed )
		local timeout = end_time - os.time( )
		local param2 = minetest.get_node( pos ).param2

		if timeout < -rainbow_timeout or timeout > 0 then
	                minetest.swap_node( pos, { name = "digital:rainbow_off", param2 = param2 } )
		end
		return true
	end,
} )


register_rainbow( "off", "flasher_off.png", 0.0, {
	on_timer = function( pos, elapsed )
		local timeout = end_time - os.time( )
		local param2 = minetest.get_node( pos ).param2

		if timeout <= 0 and timeout >= -rainbow_timeout then
	                minetest.swap_node( pos, { name = "digital:rainbow_on", param2 = param2 } )
		end
		return true
	end,
	after_place_node = function( pos, player, itemstack )
		minetest.get_node_timer( pos ):start( 1 )
	end,
} )

register_flasher( "on", "flasher_on.png", 5.0, {
	light_source = 10,
	drop = "digital:flasher_off",
	on_timer = function ( pos, elapsed )
		local timeout = end_time - os.time( )
		local param2 = minetest.get_node( pos ).param2

		if timeout < -flasher_timeout or timeout > 0 then
	                minetest.swap_node( pos, { name = "digital:flasher_off", param2 = param2 } )
		end
		return true
	end,
} )

register_flasher( "off", "flasher_off.png", 0.0, {
	on_timer = function( pos, elapsed )
		local timeout = end_time - os.time( )
		local param2 = minetest.get_node( pos ).param2

		if timeout <= 0 and timeout >= -flasher_timeout then
	                minetest.swap_node( pos, { name = "digital:flasher_on", param2 = param2 } )
		end
		return true
	end,
	after_place_node = function( pos, player, itemstack )
		minetest.get_node_timer( pos ):start( 1 )
	end,
} )

----------------------------------------------------------------------------

local globe_texture_off = "digital:globe_base"
local globe_texture_on = "digital:globe_base"
local globe_pos = { x = -5.5, y = 10, z = 0 }

local show_globe = false
local globe_obj

minetest.register_chatcommand( "counter", {
        description = "Initialize the countdown clock given seconds remaining.",
        privs = { server = true },
        func = function( name, param )
		if param ~= "" then
			rem_time = math.max( 0, tonumber( param ) )
			end_time = os.time( ) + tonumber( param )
			minetest.chat_send_player( name, string.format( "%d seconds remaining on countdown clock.", rem_time ) )

			if globe_obj then
				minetest.after( rem_time - 10, globe_obj:get_luaentity( ).motion_drop )
			end
		else
			end_time = 0
		end
	end
} )

minetest.register_node( "digital:globe_base", {
        wield_image = "globe.png",
        paramtype = "light",
	light_source = 10,
} )

minetest.register_entity( "digital:globe",{
        hp_max = 1,
        visual = "wielditem",
        visual_size = { x=1.5, y=1.5 },
        collisionbox = { 0,0,0,0,0,0 },
        physical = false,
        textures = { "air" },
        on_activate = function( self, staticdata )
		if show_globe then
			local yaw = math.pi / 2
			self.object:set_yaw( yaw )
                	self.object:set_properties({textures={globe_texture_off}})
		else
			self.object:remove( )
		end
        end,
	motion_drop = function( )
print( "start" )
		globe_obj:set_velocity( { x = 0, y = 0.5, z = 0 } )
		minetest.after( 10, globe_obj:get_luaentity( ).motion_stop )

		minetest.after( 5, minetest.chat_send_all, "*** FIVE!" )
		minetest.after( 6, minetest.chat_send_all, "*** FOUR!" )
		minetest.after( 7, minetest.chat_send_all, "*** THREE!" )
		minetest.after( 8, minetest.chat_send_all, "*** TWO!" )
		minetest.after( 9, minetest.chat_send_all, "*** ONE!" )
		minetest.after( 10, minetest.chat_send_all, "*** HAPPY NEW YEAR EVERYBODY!!!" )
		minetest.after( 35, minetest.chat_send_all, "*** CONGRATULATIONS! 2019 IS HERE!!!" )
	end,
	motion_stop = function ( )
print( "stop" )
		globe_obj:set_velocity( { x = 0, y = 0, z = 0 } )
		minetest.sound_play( "auldangesyne2", { gain = 0.5, loop = false } )

		minetest.after( 26, function ( )
	                local sound_ptr = minetest.sound_play( "welcome4", { gain = 0.5, loop = true } )
			minetest.after( 41, function ( )
				minetest.sound_stop( sound_ptr )
			end )
                end )
	end,
        get_staticdata = function(self)
                return ""
        end,
})

minetest.register_chatcommand( "globe", {
        description = "Activate or deactivate the countdown globe.",
        privs = { server = true },
        func = function( name, param )
		show_globe = not show_globe
		if show_globe then
			globe_obj = minetest.env:add_entity( globe_pos, "digital:globe" )
		globe_obj:set_velocity( { x = 0, y = 0, z = 0 } )
		else
			globe_obj:remove( )
		end
	end
} )
