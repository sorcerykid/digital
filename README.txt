Digital LED Mod v1.2
By Leslie E. Krause

Digital LED provides animated LED nodes for Minetest such as counters, color washes,
and white washes in addition to a giant Mese Globe for synchronized visual effects 
during new years eve countdowns in multiplayer games.


Source Code License
----------------------

MIT License

Copyright (c) 2016-2019, Leslie E. Krause.

Permission is hereby granted, free of charge, to any person obtaining a copy of this
software and associated documentation files (the "Software"), to deal in the Software
without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE
FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

For more details:
https://opensource.org/licenses/MIT


Multimedia License (textures, sounds, and models)
----------------------------------------------------------

Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)

	/textures/counter_0.png
	by sorcerykid

	/textures/counter_1.png
	by sorcerykid

	/textures/counter_2.png
	by sorcerykid

	/textures/counter_3.png
	by sorcerykid

	/textures/counter_4.png
	by sorcerykid

	/textures/counter_5.png
	by sorcerykid

	/textures/counter_6.png
	by sorcerykid

	/textures/counter_7.png
	by sorcerykid

	/textures/counter_8.png
	by sorcerykid

	/textures/counter_9.png
	by sorcerykid

	/textures/counter_nil.png
	by sorcerykid

	/textures/counter_off.png
	by sorcerykid

	/textures/counter_sep.png
	by sorcerykid

	/textures/digital_box.png
	by sorcerykid

	/textures/flasher_inv.png
	by sorcerykid

	/textures/flasher_off.png
	by sorcerykid

	/textures/flasher_on.png
	by sorcerykid

	/textures/globe2.png
	by sorcerykid

	/textures/globe.png
	by sorcerykid

	/textures/rainbow_inv.png
	by sorcerykid

	/textures/rainbow_on.png
	by sorcerykid

You are free to:
Share — copy and redistribute the material in any medium or format.
Adapt — remix, transform, and build upon the material for any purpose, even commercially.
The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and
indicate if changes were made. You may do so in any reasonable manner, but not in any way
that suggests the licensor endorses you or your use.

No additional restrictions — You may not apply legal terms or technological measures that
legally restrict others from doing anything the license permits.

Notices:

You do not have to comply with the license for elements of the material in the public
domain or where your use is permitted by an applicable exception or limitation.
No warranties are given. The license may not give you all of the permissions necessary
for your intended use. For example, other rights such as publicity, privacy, or moral
rights may limit how you use the material.

For more details:
http://creativecommons.org/licenses/by-sa/3.0/
